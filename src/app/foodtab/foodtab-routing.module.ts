import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FoodtabPage } from './foodtab.page';

const routes: Routes = [
  {
    path: '',
    component: FoodtabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoodtabPageRoutingModule {}
