import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabtacoPage } from './tabtaco.page';

const routes: Routes = [
  {
    path: '',
    component: TabtacoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabtacoPageRoutingModule {}
