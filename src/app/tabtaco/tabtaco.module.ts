import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabtacoPageRoutingModule } from './tabtaco-routing.module';

import { TabtacoPage } from './tabtaco.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabtacoPageRoutingModule
  ],
  declarations: [TabtacoPage]
})
export class TabtacoPageModule {}
