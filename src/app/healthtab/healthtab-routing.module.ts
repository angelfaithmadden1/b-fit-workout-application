import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HealthtabPage } from './healthtab.page';

const routes: Routes = [
  {
    path: '',
    component: HealthtabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HealthtabPageRoutingModule {}
