import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

//variables tapos yung Todo ay yung pang export to other files

export interface Todo {
  task:string;
  priority:number;
  createdAt:number;
}



@Injectable({
  providedIn: 'root', 
})



export class IdeaService {
  
  private ideacollection: AngularFirestoreCollection<Todo>;

  private ideas: Observable<Todo[]>;

  constructor(db: AngularFirestore) { 
    this.ideacollection = db.collection<Todo>('ideas');

    this.ideas = this.ideacollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id,...data};
        });
      })
    ); 
  }
  getTodos() {
    return this.ideas;
  }
 
  getTodo(id) {
    return this.ideacollection.doc<Todo>(id).valueChanges();
  }
 
  updateTodo(todo: Todo, id: string) {
    return this.ideacollection.doc(id).update(todo);
  }
 
  addTodo(todo: Todo) {
    return this.ideacollection.add(todo);
  }
 
  removeTodo(id) {
    return this.ideacollection.doc(id).delete();
  }
}

