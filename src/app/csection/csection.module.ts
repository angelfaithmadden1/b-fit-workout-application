import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CsectionPageRoutingModule } from './csection-routing.module';

import { CsectionPage } from './csection.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CsectionPageRoutingModule
  ],
  declarations: [CsectionPage]
})
export class CsectionPageModule {}
