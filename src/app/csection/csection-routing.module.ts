import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CsectionPage } from './csection.page';

const routes: Routes = [
  {
    path: '',
    component: CsectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CsectionPageRoutingModule {}
