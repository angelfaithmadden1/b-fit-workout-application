import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { Wallpost, WallpostService } from '../ccomment/ccomment.service';
import { CommentPost, CommentPostService } from '../csection/csection.service';
import { AlertController } from '@ionic/angular';



@Component({
  selector: 'app-csection',
  templateUrl: './csection.page.html',
  styleUrls: ['./csection.page.scss'],
})

export class CsectionPage implements OnInit { 
  commentpostarr: CommentPost[];
  public buttonClicked: boolean = false;
  public buttonClicked2: boolean = false;
  
  wallpostID = null;
  commentpostID = null;

  wallpost: Wallpost = {
    title: '',
    comment: '',
    createdAt: new Date().getTime(),
    guestUser: '',
  }

  commentpost: CommentPost = {
    Cid: '',
    comment: '',
    createdAt: new Date().getTime()
  }


  constructor(private wallpostService: WallpostService, private commentpostService: CommentPostService,
    private route: ActivatedRoute, 
    private loadingController: LoadingController, private nav: NavController,public alertController: AlertController) { }

  ngOnInit() {
    this. wallpostID = this.route.snapshot.params['id'];
    if(this.wallpostID){
      this.loadPost();
    }
   this.commentpostService.getComment().subscribe(res => {
    this.commentpostarr = res;
   });
   
  }

  async loadPost() {
    const loading = await this.loadingController.create({
      message: 'Loading Comments..'
    });
    await loading.present();

    this.wallpostService.getPost(this.wallpostID).subscribe(res => {
      loading.dismiss();
      this.wallpost = res;
      this.commentpost.Cid = this.wallpostID;
    });
  }

  async savePost() {
    const loading = await this.loadingController.create({
      message: 'Posting Comment..'
    });
    await loading.present();

    if (this.wallpostID) {
      this.wallpostService.updatePost(this.wallpost, this.wallpostID).then(() => {
        loading.dismiss();
        this.nav.navigateBack('home');
      });
    } else {
      this.wallpostService.addPost(this.wallpost).then(() => {
        loading.dismiss();
        this.nav.navigateBack('home');
      });
    }
  }

  onButtonClick() {
    this.buttonClicked = !this.buttonClicked;
    this.commentpost.comment = "";
  }

  async saveComment() {
    if(this.commentpost.comment.trim() == "") {
     this.presentAlert();
    } else {
      this.commentpostService.addComment(this.commentpost).then(() => {
      loading.dismiss();
      this.onButtonClick();
      //this.nav.navigateBack('home');
      })
      const loading = await this.loadingController.create({
        message: 'Posting Comment..'
      });
      await loading.present();
    }
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Warning',
      message: 'Comment field is empty.',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
}
