import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

//variables tapos yung Todo ay yung pang export to other files

export interface CommentPost{
  Cid: string;
  comment: string;
  createdAt: number;
}

@Injectable({
  providedIn: 'root', 
})

//same as dito
export class CommentPostService{
  private commentPostCollection: AngularFirestoreCollection<CommentPost>;

  private commentpost: Observable<CommentPost[]>;

  constructor(db: AngularFirestore) { 
    this.commentPostCollection = db.collection<CommentPost>('commentpost');

    this.commentpost = this.commentPostCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id,...data};
        });
      })
    ); 
  }
  
  getComment() {
    return this.commentpost;
  }
 
  addComment(wallpost: CommentPost) {
    return this.commentPostCollection.add(wallpost);
  }
 

}

