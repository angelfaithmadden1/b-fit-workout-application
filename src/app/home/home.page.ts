import { Component, OnInit } from '@angular/core';
import { Wallpost, WallpostService } from '../ccomment/ccomment.service';
;

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
    wallpost: Wallpost[];

  constructor(private wallPostService: WallpostService) { 

  }

  ngOnInit() {
    this.wallPostService.getPosts().subscribe(res => {
      this.wallpost = res;
    });
  }   

  remove(item){
    this.wallPostService.removePost(item.id);
  }

  transferto(item) {
    this.wallPostService.getPost(item.id);
  }

}
