import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { Wallpost, WallpostService } from '../ccomment/ccomment.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-ccomment',
  templateUrl: './ccomment.page.html',
  styleUrls: ['./ccomment.page.scss'],
})
export class CcommentPage implements OnInit {

  wallpost: Wallpost = {
    title: '',
    comment: '',
    createdAt: new Date().getTime(),
    guestUser: '',
  }

  wallpostID = null;

  constructor(private wallpostService: WallpostService, private route: ActivatedRoute, 
    private loadingController: LoadingController, private nav: NavController, public alertController: AlertController) { }

  ngOnInit() {
    this. wallpostID = this.route.snapshot.params['id'];
    if(this.wallpostID){
      this.loadPost();
    }
  }

  async loadPost() {
    const loading = await this.loadingController.create({
      message: 'Loading Todo..'
    });
    await loading.present();

    this.wallpostService.getPost(this.wallpostID).subscribe(res => {
      loading.dismiss();
      this.wallpost = res;
    });
  }

  async savePost() {
    if (this.wallpost.title.trim() == "") {   
      this.presentAlert();
    } else if (this.wallpost.comment.trim() == "") {
      this.presentAlert2();
    }
    else {
      const loading = await this.loadingController.create({
        message: 'Saving Todo..'
      });
      await loading.present();
  
      if (this.wallpostID) {
        this.wallpostService.updatePost(this.wallpost, this.wallpostID).then(() => {
          loading.dismiss();
          this.nav.navigateBack('home');
        });
      } else {
        this.wallpostService.addPost(this.wallpost).then(() => {
          loading.dismiss();
          this.nav.navigateBack('home');
        });
      }
    }
  }


  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Warning',
      message: 'Title field is empty.',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Warning',
      message: 'Comment field is empty.',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

}
