import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CcommentPageRoutingModule } from './ccomment-routing.module';

import { CcommentPage } from './ccomment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CcommentPageRoutingModule
  ],
  declarations: [CcommentPage]
})
export class CcommentPageModule {}
