import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

//variables tapos yung Todo ay yung pang export to other files

export interface Wallpost{
  title: string;
  comment: string;
  createdAt: number;
  guestUser: string; 
}

@Injectable({
  providedIn: 'root', 
})

//same as dito
export class WallpostService{
  private wallpostcollection: AngularFirestoreCollection<Wallpost>;

  private wallpost: Observable<Wallpost[]>;

  constructor(db: AngularFirestore) { 
    this.wallpostcollection = db.collection<Wallpost>('wallpost');

    this.wallpost = this.wallpostcollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id,...data};
        });
      })
    ); 
  }
  
  getPosts() {
    return this.wallpost;
  }
 
  getPost(id) {
    return this.wallpostcollection.doc<Wallpost>(id).valueChanges();
  }
 
  updatePost(wallpost: Wallpost, id: string) {
    return this.wallpostcollection.doc(id).update(wallpost);
  }
 
  addPost(wallpost: Wallpost) {
    return this.wallpostcollection.add(wallpost);
  }
 
  removePost(id) {
    return this.wallpostcollection.doc(id).delete();
  }
}

