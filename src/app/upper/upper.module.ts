import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpperPageRoutingModule } from './upper-routing.module';

import { UpperPage } from './upper.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpperPageRoutingModule
  ],
  declarations: [UpperPage]
})
export class UpperPageModule {}
