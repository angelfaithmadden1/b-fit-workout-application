import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LowerPageRoutingModule } from './lower-routing.module';

import { LowerPage } from './lower.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LowerPageRoutingModule
  ],
  declarations: [LowerPage]
})
export class LowerPageModule {}
