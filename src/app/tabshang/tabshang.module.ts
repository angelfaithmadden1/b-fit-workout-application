import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabshangPageRoutingModule } from './tabshang-routing.module';

import { TabshangPage } from './tabshang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabshangPageRoutingModule
  ],
  declarations: [TabshangPage]
})
export class TabshangPageModule {}
