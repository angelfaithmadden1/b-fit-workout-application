import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HealthPage } from './health.page';

const routes: Routes = [
  {
    path: '',
    component: HealthPage,
    children: [
      {
        path: 'healthtab',
        loadChildren: () => import('../healthtab/healthtab.module').then( m => m.HealthtabPageModule)
      },
      {
        path: 'foodtab',
        loadChildren: () => import('../foodtab/foodtab.module').then( m => m.FoodtabPageModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HealthPageRoutingModule {}
